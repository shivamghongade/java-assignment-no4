import java.util.Scanner;

 class PatternDemo {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter the number of rows: ");
        int numRows = sc.nextInt();
        
        System.out.print("Enter the characters to print (e.g., A B C D): ");
        String characters = sc.next();
        
        for (int i = 0; i < numRows; i++) {
            for (char character : characters.toCharArray()) {
                System.out.print(character + " ");
            }
            System.out.println();
            
            for (int j = 0; j < characters.length(); j++) {
                System.out.print("# ");
            }
            System.out.println();
        }
    }
}

