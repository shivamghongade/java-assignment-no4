import java.util.Scanner;

 class PatternDemo {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        
        System.out.print("Enter the number of rows: ");
        int numRows = sc.nextInt();
        
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j <= i; j++) {
                char character = (char) ('A' + i - j);
                System.out.print(character + " ");
            }
            System.out.println();
        }
    }
}
