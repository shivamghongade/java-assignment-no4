 class ToggleCase {

    public static void main(String[] args) {

        String input = "input data";

              String toggled = toggleCase(input);
                   System.out.println("Input: " + input);
        System.out.println("Toggled: " + toggled);
    }

    public static String toggleCase(String str) {
       
        if (!str.isEmpty() && Character.isLowerCase(str.charAt(0))) {
            return str.toUpperCase();
        } else {
            return str.toLowerCase();
        }
    }
}
